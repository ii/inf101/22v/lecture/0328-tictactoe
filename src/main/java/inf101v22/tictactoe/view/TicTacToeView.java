package inf101v22.tictactoe.view;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import inf101v22.tictactoe.controller.TicTacToeController;
import inf101v22.tictactoe.model.TicTacToeViewable;

/**
 * The TicTacToeView is a JPanel responsible for displaying an entire TicTacToe
 * application, and is a such a "root" container.
 */
public class TicTacToeView extends JPanel {
    private static final int OUTER_PADDING = 15;

    private GridView gridView;
    private TopLine topLine;

    /**
     * Creates a new TicTacToeView.
     * 
     * @param model      model to be drawn
     * @param controller controller
     */
    public TicTacToeView(TicTacToeViewable model, TicTacToeController controller) {
        // Using a vertical box layout, we bind together the three panels used
        // for the tic tac toe game: the top line displaying text, the grid view,
        // and a reset button at the bottom.
        this.gridView = new GridView(model, controller);
        this.topLine = new TopLine(model, controller);
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        JButton resetButton = new JButton("Reset");
        resetButton.setFocusable(false);
        resetButton.setAlignmentX(JButton.CENTER_ALIGNMENT);
        resetButton.addActionListener(controller::reset);

        this.add(this.topLine);
        this.add(Box.createRigidArea(new Dimension(0, 5)));
        this.add(this.gridView);
        this.add(Box.createRigidArea(new Dimension(0, 15)));
        this.add(resetButton);
        this.setBorder(BorderFactory.createEmptyBorder(OUTER_PADDING, OUTER_PADDING, OUTER_PADDING, OUTER_PADDING));
    }

}
