package inf101v22.tictactoe.view;

import java.awt.Graphics;
import java.util.function.Function;
import java.awt.Dimension;

import javax.swing.JPanel;

import inf101v22.grid.Grid;
import inf101v22.grid.InspectableGrid;
import inf101v22.grid.Location;
import inf101v22.tictactoe.controller.TicTacToeController;
import inf101v22.tictactoe.model.TicTacToeViewable;

/**
 * A GridView is responsible for drawing a grid from a TicTacToeViewable
 */
public class GridView extends JPanel {
    private static final int MARGIN = 5;

    private final InspectableGrid<TileView> viewGrid;

    GridView(TicTacToeViewable model, TicTacToeController controller) {

        // A function which takes as input a Location, and produce a TileView object
        Function<Location, TileView> tileViewProducer = (Location location) -> {
            TileView tileView = new TileView(location, model, controller);
            GridView.this.add(tileView);
            return tileView;
        };

        // Create a grid of TileView -objects, matching one-to-one with the locations in the board of the model.
        this.viewGrid = new Grid<TileView>(model.getBoard().numRows(), model.getBoard().numCols(), tileViewProducer);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        // Compute the bounds for child components
        final int panelWidth = this.getWidth() + MARGIN;
        final int panelHeight = this.getHeight() + MARGIN;
        final int cols = this.viewGrid.numCols();
        final int rows = this.viewGrid.numRows();

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                int x = col * panelWidth / cols;
                int nx = (col + 1) * panelWidth / cols;
                int width = nx - x - MARGIN;

                int y = row * panelHeight / rows;
                int ny = (row + 1) * panelHeight / rows;
                int height = ny - y - MARGIN;

                this.viewGrid.get(new Location(row, col)).setBounds(x, y, width, height);;
            }
        }
    }

    @Override
    public Dimension preferredSize() {
        int preferredWidth = 400;
        int preferredHeight = 400;
        return new Dimension(preferredWidth, preferredHeight);
    }
    
}
