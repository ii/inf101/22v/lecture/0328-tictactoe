package inf101v22.tictactoe.view;

import java.awt.Graphics;
import java.util.Objects;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.plaf.FontUIResource;

import inf101v22.observable.Observable;
import inf101v22.tictactoe.controller.TicTacToeController;
import inf101v22.tictactoe.model.TicTacToeModel;
import inf101v22.tictactoe.model.TicTacToeViewable;
import inf101v22.viewutils.GraphicHelperMethods;

public class TopLine extends JPanel {

    private Observable<Boolean> isGameOver;
    private Observable<Character> currentPlayer;
    private Observable<Character> winningPlayer;

    private String message = "";

    public TopLine(TicTacToeViewable model, TicTacToeController controller) {
        this.isGameOver = model.isGameOver();
        this.currentPlayer = model.getCurrentPlayer();
        this.winningPlayer = model.getWinningPlayer();

        this.isGameOver.addObserver(this::refresh);
        this.currentPlayer.addObserver(this::refresh);
        this.winningPlayer.addObserver(this::refresh);
        this.refresh();
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // g.setColor(Color.PINK);
        // g.fillRect(0, 0, getWidth(), getHeight());

        g.setFont(new FontUIResource("Sans Serif", FontUIResource.BOLD, 16));
        g.setColor(Color.BLACK);
        GraphicHelperMethods.drawCenteredString(g, this.message, 0, 0, getWidth(), getHeight());
    }


    private void refresh() {
        if (this.isGameOver.getValue()) {
            Character winningPlayer = this.winningPlayer.getValue();
            if (Objects.equals(winningPlayer, TicTacToeModel.BLANK)) {
                this.message = "It's a draw";
            } else {
                this.message = "Player " + winningPlayer + " won!";
            }
        } else {
            this.message = "It's player " + this.currentPlayer.getValue() + "'s turn";
        }
        this.repaint();
    }

    @Override
    public Dimension preferredSize() {
        int preferredWidth = 400;
        int preferredHeight = 50;
        return new Dimension(preferredWidth, preferredHeight);
    }
    
}
