package inf101v22.tictactoe.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Objects;

import javax.swing.JPanel;
import javax.swing.plaf.FontUIResource;

import inf101v22.grid.Location;
import inf101v22.observable.Observable;
import inf101v22.tictactoe.controller.TicTacToeController;
import inf101v22.tictactoe.model.TicTacToeViewable;
import inf101v22.viewutils.GraphicHelperMethods;

/**
 * The TileView class is a JPanel responsible for drawing a single tile in the
 * grid. This is a component which receives user input through mouse
 * interactions.
 */
public class TileView extends JPanel implements MouseListener {
    private static final Color COLOR_DEFAULT = Color.GRAY.darker();
    private static final Color COLOR_X = Color.GREEN.darker().darker().darker().darker();
    private static final Color COLOR_O = Color.BLUE.darker().darker().darker().darker();
    private static final Color COLOR_X_MAYBE = GraphicHelperMethods.mixColors(COLOR_X, COLOR_DEFAULT, COLOR_DEFAULT);
    private static final Color COLOR_O_MAYBE = GraphicHelperMethods.mixColors(COLOR_O, COLOR_DEFAULT, COLOR_DEFAULT);
    private static final Color TEXT_COLOR = Color.WHITE;

    // Fixed properties of this object
    private final TicTacToeViewable model;
    private final TicTacToeController controller;
    private final Location location;
    private final Observable<Character> valueOfThisLocationInModelGrid;

    // View-related state
    private boolean mouseIsOver = false;
    private Color backgroundColor = COLOR_DEFAULT;

    /**
     * Constructs a new TileView representing the given location in the the given model.
     * 
     * @param location location this represents
     * @param model model in which this represents the given location
     * @param controller controller object this view will forward any received user interactions to
     */
    public TileView(Location location, TicTacToeViewable model, TicTacToeController controller) {
        this.location = location;
        this.model = model;
        this.controller = controller;
        this.valueOfThisLocationInModelGrid = model.getBoard().get(location);
        this.init();
        this.refresh();
    }

    /** Initialize the observables by adding appropriate observers/listeners */
    private void init() {
        this.valueOfThisLocationInModelGrid.addObserver(this::refresh); // When value of observable changes, call refresh
        this.addMouseListener(this); // When mouse interacts with this panel, handle it here
    }

    /** Recalculates view-related state and repaints the component. */
    private void refresh() {
        char currentState = this.valueOfThisLocationInModelGrid.getValue();

        this.setCursor(new Cursor(switch (currentState) {
            case '-' -> this.model.isGameOver().getValue() ? Cursor.DEFAULT_CURSOR : Cursor.HAND_CURSOR;
            default -> Cursor.DEFAULT_CURSOR;
        }));

        this.backgroundColor = switch (currentState) {
            case 'X' -> COLOR_X;
            case 'O' -> COLOR_O;
            default -> this.mouseIsOver && !this.model.isGameOver().getValue()
                    ? switch (this.model.getCurrentPlayer().getValue()) {
                        case 'X' -> COLOR_X_MAYBE;
                        case 'O' -> COLOR_O_MAYBE;
                        default -> COLOR_DEFAULT;
                    }
                    : COLOR_DEFAULT;
        };

        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(this.backgroundColor);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        g.setColor(TEXT_COLOR);
        g.setFont(new FontUIResource("Sans Serif", FontUIResource.BOLD, 20));
        String msg = Objects.equals(this.valueOfThisLocationInModelGrid.getValue(), '-') ? "" : "" + this.valueOfThisLocationInModelGrid.getValue();
        GraphicHelperMethods.drawCenteredString(g, msg, 0, 0, this.getWidth(), this.getHeight());
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        this.controller.locationClicked(this.location);
        // call to this.refresh(); is unnecessary, since changes in the model will automatically call refresh.
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // ignore
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // ignore
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        this.mouseIsOver = true;
        this.refresh();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.mouseIsOver = false;
        this.refresh();
    }

}
