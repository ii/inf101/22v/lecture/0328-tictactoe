package inf101v22.tictactoe.controller;

import java.awt.event.ActionEvent;

import inf101v22.grid.Location;
import inf101v22.tictactoe.model.TicTacToeControllable;

public class TicTacToeController {

    private TicTacToeControllable model;

    public TicTacToeController(TicTacToeControllable model) {
        this.model = model;
    }

    /**
     * Handles when the user clicks a location on the board
     * 
     * @param location location which is clicked
     */
    public void locationClicked(Location location) {
        if (this.model.isGameOver().getValue()) {
            return;
        }
        if (this.model.setMarkAt(location)) {
            this.model.goToNextPlayer();
        }
    }

    /**
     * Handles when the user clicks the reset button.
     * 
     * @param e
     */
    public void reset(ActionEvent e) {
        this.model.reset();
    }
    
}
