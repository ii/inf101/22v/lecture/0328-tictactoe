package inf101v22.tictactoe;

import javax.swing.JFrame;

import inf101v22.tictactoe.controller.TicTacToeController;
import inf101v22.tictactoe.model.TicTacToeModel;
import inf101v22.tictactoe.view.TicTacToeView;

public class Main {
    
    public static void main(String[] args) {
        TicTacToeModel model = new TicTacToeModel();
        TicTacToeController controller = new TicTacToeController(model);
        TicTacToeView view = new TicTacToeView(model, controller);

        JFrame frame = new JFrame("INF101 Tic Tac Toe");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(view);
        frame.pack();
        frame.setVisible(true);
    }

}
