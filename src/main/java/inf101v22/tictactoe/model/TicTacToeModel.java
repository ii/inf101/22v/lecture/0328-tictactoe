package inf101v22.tictactoe.model;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import inf101v22.grid.ControlledGrid;
import inf101v22.grid.GridDirection;
import inf101v22.grid.GridUtilities;
import inf101v22.grid.InspectableGrid;
import inf101v22.grid.Location;
import inf101v22.observable.ControlledValue;
import inf101v22.observable.Observable;

public class TicTacToeModel implements TicTacToeControllable {
    public static final Character BLANK = '-';
    public static final Character PLAYER_ONE = 'X';
    public static final Character PLAYER_TWO = 'O';

    private static final List<Character> players = Arrays.asList(PLAYER_ONE, PLAYER_TWO);

    // The current state of the TicTacToe game
    private final ControlledGrid<Character> board = new ControlledGrid<>(3, 3, BLANK);
    private final ControlledValue<Character> currentPlayer = new ControlledValue<>(PLAYER_ONE);

    // Calculated values
    private final ControlledValue<Boolean> isGameOver = new ControlledValue<Boolean>(false);
    private final ControlledValue<Character> winningPlayer = new ControlledValue<Character>(BLANK);

    // Methods for viewing the model

    @Override
    public InspectableGrid<? extends Observable<Character>> getBoard() {
        return this.board.getObservableGrid();
    }

    @Override
    public Observable<Character> getCurrentPlayer() {
        return this.currentPlayer;
    }

    @Override
    public Observable<Boolean> isGameOver() {
        return this.isGameOver;
    }

    @Override
    public Observable<Character> getWinningPlayer() {
        return this.winningPlayer;
    }

    // Methods for controlling the model

    @Override
    public void goToNextPlayer() {
        int currIndex = players.indexOf(this.currentPlayer.getValue());
        int nextIndex = (currIndex + 1) % players.size();
        Character nextPlayer = players.get(nextIndex);
        this.currentPlayer.setValue(nextPlayer);
    }

    @Override
    public boolean setMarkAt(Location location) {
        if (Objects.equals(board.get(location), BLANK)) {
            this.board.set(location, this.currentPlayer.getValue());
            this.checkGameOver();
            return true;
        }
        return false;
    }

    private void checkGameOver() {
        for (Character player : players) {
            List<Location> longestLine = GridUtilities.longestLineOfElement(this.board, player,
                    GridDirection.EAST_TO_SOUTHWEST);
            if (longestLine.size() >= 3) {
                this.winningPlayer.setValue(player);
                this.isGameOver.setValue(true);
            }
        }
        if (!this.board.contains(BLANK)) {
            this.isGameOver.setValue(true);
        }
    }

    @Override
    public void reset() {
        this.board.fill(BLANK);
        this.currentPlayer.setValue(PLAYER_ONE);
        this.isGameOver.setValue(false);
        this.winningPlayer.setValue(BLANK);
    }

}
