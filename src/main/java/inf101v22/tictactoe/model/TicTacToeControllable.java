package inf101v22.tictactoe.model;

import inf101v22.grid.Location;

public interface TicTacToeControllable extends TicTacToeViewable {
    
    /** Goes to the next player. */
    void goToNextPlayer();

    /**
     *  Sets a mark at the given location
     * 
     * @param location location to be marked, non-null
     * @return true if the location was marked, false if the location was already occupied.
     */
    boolean setMarkAt(Location location);

    /** Resets the game */
    void reset();
}
