package inf101v22.tictactoe.model;

import inf101v22.grid.InspectableGrid;
import inf101v22.observable.Observable;

public interface TicTacToeViewable {
    
    /** Gets an inspectable and observable version of the game board. */
    InspectableGrid<? extends Observable<Character>> getBoard();

    /** Gets an observable of the current player. */
    Observable<Character> getCurrentPlayer();

    /** Gets an observable of whether the game is over. */
    Observable<Boolean> isGameOver();

    /** Gets an observable of which player is winning. */
    Observable<Character> getWinningPlayer();
}
