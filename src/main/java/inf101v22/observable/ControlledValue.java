package inf101v22.observable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ControlledValue<E> implements Observable<E> {
    private List<Observer> watchers = new ArrayList<>();
    private E value;

    /** Creates a new ContolledValue with the initial value. */
    public ControlledValue(E initialValue) {
        this.value = initialValue;
    }

    @Override
    public E getValue() {
        return this.value;
    }

    @Override
    public void addObserver(Observer listener) {
        this.watchers.add(listener);
    }

    @Override
    public boolean removeObserver(Observer listener) {
        return this.watchers.remove(listener);
    }

    /**
     * Sets the value of this ControlledValue to the newValue. All watchers will be
     * notified unless newValue is .equal to the current value, in which case the
     * object will not be updated (even if they are actually not the same object).
     * 
     * @param newValue new value
     */
    public boolean setValue(E newValue) {
        E oldValue = this.value;
        if (!Objects.equals(newValue, oldValue)) {
            this.value = newValue;
            this.onValueChanged();
            return true;
        }
        return false;
    }

    private void onValueChanged() {
        for (Observer changeListener : watchers) {
            changeListener.update();
        }
    }

}
