package inf101v22.observable;

public interface Observable<E> {

    /** Gets the current value */
    E getValue();

    /** Adds an observer */
    void addObserver(Observer observer);

    /**
     * Removes an observer. Returns true if the observer was removed, and false if
     * an observer was not found.
     */
    boolean removeObserver(Observer observer);

}
