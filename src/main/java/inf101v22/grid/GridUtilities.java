package inf101v22.grid;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Helper methods for grids
 */
public class GridUtilities {

    /**
     * Gets a list of locations in the grid all containing the given element.
     * The returned list will be in one of the provided directions, and will be
     * a longest such list of those that exists.
     * 
     * @param <E>
     * @param grid       grid to search
     * @param element    element to be searched for
     * @param directions directions to search
     * @return a longest list of locations that all contains the element, and which
     *         are consecutive according to one of the given directions
     */
    public static <E> List<Location> longestLineOfElement(InspectableGrid<E> grid, E element,
            GridDirection... directions) {
        List<Location> result = new ArrayList<>();
        for (GridDirection gridDirection : directions) {
            for (Location startingFrom : grid.locations()) {
                List<Location> candidate = longestLineOfElement(grid, element, startingFrom, gridDirection,
                        new ArrayList<>());
                result = result.size() < candidate.size() ? candidate : result;
            }
        }

        return result;
    }

    private static <E> List<Location> longestLineOfElement(InspectableGrid<E> grid, E element, Location startingFrom,
            GridDirection direction, List<Location> result) {
        if (!grid.locationIsOnGrid(startingFrom) || !Objects.equals(element, grid.get(startingFrom))) {
            return result;
        }
        result.add(startingFrom);
        return longestLineOfElement(grid, element, direction.goFrom(startingFrom), direction, result);
    }

}
