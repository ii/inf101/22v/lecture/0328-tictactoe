package inf101v22.grid;

public enum GridDirection {
    CENTER(0, 0),
    NORTH(-1, 0),
    SOUTH(1, 0),
    EAST(0, 1),
    WEST(0, -1),
    NORTHEAST(-1, 1),
    NORTHWEST(-1, -1),
    SOUTHEAST(1, 1),
    SOUTHWEST(1, -1);

    /** The four directions NORTH, SOUTH, EAST and WEST */
    public static final GridDirection[] FOUR_DIRECTIONS = { NORTH, SOUTH, EAST, WEST };

    /** The five directions NORTH, SOUTH, EAST, WEST and CENTER */
    public static final GridDirection[] FIVE_DIRECTIONS = { NORTH, SOUTH, EAST, WEST, CENTER };

    /**
     * The eight directions NORTH, SOUTH, EAST, WEST, NORTHEAST, NORTHWEST,
     * SOUTHEAST and SOUTHWEST
     */
    public static final GridDirection[] EIGHT_DIRECTIONS = { NORTH, SOUTH, EAST, WEST, NORTHEAST, NORTHWEST, SOUTHEAST,
            SOUTHWEST };
    /**
     * The nine directions NORTH, SOUTH, EAST, WEST, NORTHEAST, NORTHWEST,
     * SOUTHEAST, SOUTHWEST and CENTER
     */
    public static final GridDirection[] NINE_DIRECTIONS = { NORTH, SOUTH, EAST, WEST, NORTHEAST, NORTHWEST, SOUTHEAST,
            SOUTHWEST, CENTER };

    /**
     * The four directions EAST, SOUTHEAST, SOUTH and SOUTHWEST.
     */
    public static final GridDirection[] EAST_TO_SOUTHWEST = { EAST, SOUTHEAST, SOUTH, SOUTHWEST };

    public final int dRow, dCol;

    private GridDirection(int dRow, int dCol) {
        this.dRow = dRow;
        this.dCol = dCol;
    }

    /**
     * Gets the loctation found by moving this direction from the given location.
     * 
     * @param location from where to start, non-null
     * @return location found by moving one step in this direction
     */
    public Location goFrom(Location location) {
        return new Location(location.row + this.dRow, location.col + this.dCol);
    }

}