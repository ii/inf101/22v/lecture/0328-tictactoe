package inf101v22.grid;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * A traditional grid.
 */
public class Grid<E> implements EditableGrid<E> {
    private final int rows;
    private final int cols;
    private final List<E> items;

    /** Creates a new grid with the given dimensions, initially filled with null. */
    public Grid(int rows, int cols) {
        this(rows, cols, (E) null);
    }

    /**
     * Creates a new grid with the given dimensions, initially filled with the given
     * value.
     */
    public Grid(int rows, int cols, E initialValue) {
        this(rows, cols, (ignored) -> initialValue);
    }

    /**
     * Creates a new grid with the given dimensions, where the initial values are
     * produced by the provided function.
     */
    public Grid(int rows, int cols, Function<Location, E> initialValueProducer) {
        if (rows <= 0 || cols <= 0) {
            throw new IllegalArgumentException(
                    "Attempted to create a grid with non-positive dimensions '" + rows + "x" + cols + "'");
        }
        this.rows = rows;
        this.cols = cols;
        this.items = new ArrayList<>(rows * cols);
        for (int i = 0; i < rows * cols; i++) {
            this.items.add(null);
        }
        this.fill(initialValueProducer);
    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numCols() {
        return this.cols;
    }

    @Override
    public E get(Location location) {
        this.verifyLocation(location);
        return this.items.get(locationToIndex(location));
    }

    @Override
    public void set(Location location, E value) {
        this.verifyLocation(location);
        this.items.set(locationToIndex(location), value);
    }

    private int locationToIndex(Location location) {
        return location.col + location.row * this.numCols();
    }

    private void verifyLocation(Location location) {
        if (!this.locationIsOnGrid(location)) {
            throw new IndexOutOfBoundsException(
                    "Location " + location + " is out of bounds for this " + this.rows + "x" + this.cols + " grid");
        }
    }

}
