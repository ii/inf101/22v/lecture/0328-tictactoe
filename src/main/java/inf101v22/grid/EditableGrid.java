package inf101v22.grid;

import java.util.function.Function;

/**
 * An EditableGrid is a grid which can be both inspected and
 * edited.
 */
public interface EditableGrid<E> extends InspectableGrid<E> {

    /**
     * Sets the location to the given value.
     * 
     * @param location location to set, non-null
     * @throws IndexOutOfBoundsException if location is not on the grid
     * @param value new value
     */
    void set(Location location, E value);

    /** Fills the board with the given value */
    default void fill(E value) {
        this.fill((ignored) -> value);
    }

    /** Fills the board with values created by the given function. */
    default void fill(Function<Location, E> valueProducer) {
        for (Location location : this.locations()) {
            this.set(location, valueProducer.apply(location));
        }
    }

}
