package inf101v22.grid;

import java.util.Objects;

/**
 * A Location object is an immutable container for two ints, one representing a
 * row, and the other representing a column.
 */
public class Location {

    public final int row;
    public final int col;

    /** Creates a new location. */
    public Location(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public String toString() {
        return "{" +
                " row='" + this.row + "'" +
                ", col='" + this.col + "'" +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Location)) {
            return false;
        }
        Location location = (Location) o;
        return row == location.row && col == location.col;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col);
    }

}
