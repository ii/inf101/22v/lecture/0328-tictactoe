package inf101v22.grid;

import java.util.Objects;

/**
 * A LocationItem is a (possibly) immutable container containing two values, one
 * representing som element, the other representing a coordinate.
 */
public class LocationItem<E> {

    public final E item;
    public final Location location;

    public LocationItem(Location location, E item) {
        this.item = item;
        this.location = location;
    }

    @Override
    public String toString() {
        return "{" +
                " location='" + this.location + "'" +
                ", item='" + this.item + "'" +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof LocationItem)) {
            return false;
        }
        LocationItem<?> locationItem = (LocationItem<?>) o;
        return Objects.equals(item, locationItem.item) && Objects.equals(location, locationItem.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, location);
    }
}
