package inf101v22.grid;

import java.util.function.Function;

import inf101v22.observable.ControlledValue;
import inf101v22.observable.Observable;

/**
 * A ControllableGrid is an EditableGrid with the additional property
 * that it can produce an InspectableGrid of Observable elements.
 */
public class ControlledGrid<E> implements EditableGrid<E> {

    private InspectableGrid<ControlledValue<E>> grid;

    /** Creates a new grid with the given dimensions, initially filled with null. */
    public ControlledGrid(int rows, int cols) {
        this(rows, cols, (E) null);
    }

    /**
     * Creates a new grid with the given dimensions, initially filled with the given
     * value.
     */
    public ControlledGrid(int rows, int cols, E initialValue) {
        this(rows, cols, (ignored) -> initialValue);
    }

    /**
     * Creates a new grid with the given dimensions, where the initial values are
     * produced by the provided function.
     */
    public ControlledGrid(int rows, int cols, Function<Location, E> initialValueProducer) {
        this.grid = new Grid<>(rows, cols, (location) -> new ControlledValue<>(initialValueProducer.apply(location)));
    }

    @Override
    public void set(Location location, E newValue) {
        this.grid.get(location).setValue(newValue);
    }

    @Override
    public E get(Location location) {
        return this.grid.get(location).getValue();
    }

    @Override
    public int numRows() {
        return this.grid.numRows();
    }

    @Override
    public int numCols() {
        return this.grid.numCols();
    }

    /**
     * Returns an inspectable version of the grid where each loaction on the grid
     * can be observed.
     */
    public InspectableGrid<? extends Observable<E>> getObservableGrid() {
        return this.grid;
    }

}
