package inf101v22.grid;

import java.util.Iterator;
import java.util.Objects;

/**
 * An InspectableGrid is a grid which can be inspected, but which can not be
 * edited through this interface.
 */
public interface InspectableGrid<E> extends Iterable<LocationItem<E>> {

    /** Gets the number of rows in the grid. */
    int numRows();

    /** Gets the number of columns in the grid. */
    int numCols();

    /**
     * Gets the element at the given location.
     * 
     * @param location to get, non-null
     * @throws IndexOutOfBoundsException if location is off-grid
     * @return element at the given location
     */
    E get(Location location);

    /** Checks whether the provided location is within bounds of this grid. */
    default boolean locationIsOnGrid(Location location) {
        return location.row >= 0 && location.col >= 0 && location.row < numRows() && location.col < numCols();
    }

    /** Checks whether the value is found in the grid. */
    default boolean contains(E value) {
        for (LocationItem<E> locationItem : this) {
            if (Objects.equals(value, locationItem.item)) {
                return true;
            }
        }
        return false;
    }

    /** Returns an iterable over the locations that exists in the current Grid */
    default Iterable<Location> locations() {
        return new Iterable<Location>() {
            @Override
            public Iterator<Location> iterator() {
                return new Iterator<Location>() {
                    int row = 0;
                    int col = 0;

                    @Override
                    public boolean hasNext() {
                        return row < InspectableGrid.this.numRows();
                    }

                    @Override
                    public Location next() {
                        Location location = new Location(row, col);
                        col++;
                        if (col >= InspectableGrid.this.numCols()) {
                            row++;
                            col = 0;
                        }
                        return location;
                    }
                };
            }
        };
    }

    @Override
    default Iterator<LocationItem<E>> iterator() {
        return new Iterator<LocationItem<E>>() {
            Iterator<Location> locations = InspectableGrid.this.locations().iterator();

            @Override
            public boolean hasNext() {
                return this.locations.hasNext();
            }

            @Override
            public LocationItem<E> next() {
                Location location = this.locations.next();
                return new LocationItem<E>(location, InspectableGrid.this.get(location));
            }

        };
    }

}
