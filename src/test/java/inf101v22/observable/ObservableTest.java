package inf101v22.observable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ObservableTest {

    private boolean flag;

    @Test
    void sanityTest() {
        ControlledValue<String> cv = new ControlledValue<>("Hello");
        Observable<String> observable = cv;

        assertEquals("Hello", observable.getValue());

        this.flag = false;
        observable.addObserver(() -> {
            this.flag = true;
        });

        cv.setValue("Goodbye!");
        assertEquals("Goodbye!", observable.getValue());
        assertTrue(this.flag);

    }
    
}
